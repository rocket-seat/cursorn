import React, { Component } from 'react'
import { Text, View, FlatList, TouchableOpacity } from 'react-native'
import api from '../services/Api'
import styles from './MainStyles'

class Main extends Component {
  static navigationOptions = {
    title: 'Curso RN'
  }

  state = {
    docs: [],
    page: 1,
    productInfo: {}
  }

  componentDidMount() {
    this.loadProdutos();
  }

  loadProdutos = async (page = 1) => {
    const res = await api.get('/products?page=' + page)

    const { docs, ...productInfo } = res.data

    this.setState({ docs: [...this.state.docs, ...docs], productInfo, page })
  }

  loadMore = () => {
    const { page, productInfo } = this.state

    if (page == productInfo.pages) {
      return
    }

    const pgNumber = page + 1
    this.loadProdutos(pgNumber)
  }

  renderItem = ({ item }) => (
    <View style={styles.productContainer}>
      <Text style={styles.productTitle}>{item.title}</Text>
      <Text style={styles.productDescription}>{item.description}</Text>

      <TouchableOpacity style={styles.productButton} onPress={() => this.props.navigation.navigate('Product', { product: item })}>
        <Text style={styles.productButtonText}>Acessar</Text>
      </TouchableOpacity>
    </View>
  )

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          contentContainerStyle={styles.list}
          data={this.state.docs}
          keyExtractor={item => item._id}
          renderItem={this.renderItem}
          onEndReached={this.loadMore}
          onEndReachedThreshold={0.1}
        />
      </View>
    )
  }
}

export default Main