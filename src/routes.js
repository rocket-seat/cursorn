import { createStackNavigator, createAppContainer } from 'react-navigation'

import Main from './pages/Main'
import Product from './pages/Product'

const navigator = createStackNavigator(
    {
        Main,
        Product
    },
    {
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#DA552F'
            },
            headerTintColor: '#FFF'
        }
    }
)

export default createAppContainer(navigator)